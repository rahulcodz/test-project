import db from "../db.js"

const errorHandler = (err, req, res, next) => {
  console.error(err)

  const { originalUrl } = req
  const { message } = err

  const dataToSend = [req.rawHeaders, "Rahul Panchal"]
  const errorData = {
    endpoint: originalUrl,
    error_message: dataToSend,
  }
  console.log(req)

  const q = `INSERT INTO errors_table (endpoint, error_message) VALUES (?, ?)`
  const values = [errorData.endpoint, `${errorData.error_message}`]
  db.query(q, values, (error, results) => {
    if (error) {
      console.error("Error saving error to database:", error)
    }
  })

  res.status(500).json({ error: "Internal server error" })
}

export default errorHandler
