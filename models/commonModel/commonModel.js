import db from "../../db.js" // Assuming the db.js file is in the parent directory

export const getAllCommon = callback => {
  const q = "SELECT * FROM common"

  db.query(q, (err, data) => {
    if (err) {
      return callback(err, null)
    }
    return callback(null, data)
  })
}

export const addCommon = (data, callback) => {
  const { companyName, EmployeeName, department } = data

  if (!companyName || !EmployeeName || !department) {
    return callback({ error: "Required fields are missing." }, null)
  }

  const q = `INSERT INTO common (companyName, EmployeeName, department) VALUES (?, ?, ?)`
  const values = [companyName, EmployeeName, department]

  db.query(q, values, (err, result) => {
    if (err) {
      return callback(err, null)
    }
    return callback(null, result.insertId)
  })
}
