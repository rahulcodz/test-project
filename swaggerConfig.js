import swaggerUI from "swagger-ui-express"
import YAML from "yamljs"

const swaggerJSDocs = YAML.load("api.yaml")

const options = {
  customCss: `img {content:url(\'https://fastapi.tiangolo.com/img/logo-margin/logo-teal.png\'); height:auto;} `,
  customfavIcon:
    "https://static-00.iconduck.com/assets.00/swagger-icon-512x512-halz44im.png",
  customSiteTitle: "Code Improve API Doc",
}

export const swaggerServe = swaggerUI.serve
export const swaggerSetup = swaggerUI.setup(swaggerJSDocs, options)
