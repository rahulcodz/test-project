import express from "express"
import {
  deleteUser,
  getUser,
  postUser,
  updateUser,
} from "../controllers/user.js"

import { userSchema } from "../schema/schema.js"
import { paginatedUserList } from "../controllers/pagination.js"
import {
  commonDelete,
  commonGet,
  commonGetById,
  commonPost,
  commonUpdate,
} from "../controllers/common.js"
import {
  orderDetails,
  orders,
} from "../controllers/orders/orders_management.js"
import {
    categories,
    customers,
    products,
} from "../controllers/orders/customers.js"

const router = express.Router()
router.get("/common", commonGet)
router.get("/common/:id", commonGetById)
router.post("/common", commonPost)
router.delete("/common/:id", commonDelete)
router.put("/common", commonUpdate)

router.post("/order", orders)
router.post("/order-detail", orderDetails)
router.post("/customer", customers)
router.post("/propduct", products)
router.post("/category", categories)
export default router
