// swagger.js
import swaggerJsdoc from "swagger-jsdoc"

const options = {
  definition: {
    openapi: "3.0.0",
    info: {
      title: "TEST API",
      version: "1.0.0",
      description: "Test documentation",
    },
    servers: [
      {
        url: "http://localhost:8800",
      },
    ],
    swaggerOptions: {
      authAction: {
        JWT: {
          name: "JWT",
          schema: {
            type: "apiKey",
            in: "header",
            name: "Authorization",
            description: "",
          },
          value: "Bearer <my own JWT token>",
        },
      },
    },
    security: [
      {
        bearerAuth: [],
      },
    ],
  },
  apis: ["./routes/*.js"],
}

const specs = swaggerJsdoc(options)

export default specs
